import React from 'react';
import MemberNav from './MemberNav';
import Session from './Session';

// todo : Add a nav bar which will be available across the pages.
export default function MemberHome ({firstName, logout}) {
    return (
      <div className="App">
        <MemberNav firstName={firstName} logout={logout}/>
        <header >
            <h2>Welcome {firstName()}</h2>
            <Session/>
        </header>
      </div>
    );
}
