import React from 'react';
import Error from '../Error';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Question from '../Question';
import SimpleSnackBar from './SimpleSnackBar';

//todo it will need the AuthToken from the main componenets to make an API call.
export default class Session extends React.Component {
    state = {
        qty: '10',
        current: 0,
        questions: [],
        sessionStarted: false,
        sessionCompleted:false,
        responseNotification : null,
        error: null
    }

    onSessionStart = () => {
        console.log("calling the APi")
        fetch(`/questions/default?qty=${encodeURIComponent(this.state.qty)}`, {
            method: 'GET'
        }).then(res => res.json())
        .then(this.handleQuestionsResponse)
    }

    handleQuestionsResponse = (data) => {
        if (data.status !== 200) {
            this.setState({ error: "Some Error Occured" })
            return
        }
        console.log(data)
        this.setState({ questions: data.message, sessionStarted: true })
    }

    handleQuantityChange = (e) =>{
        this.setState({[e.target.name]: e.target.value})
    }

    incrementCurrentQuestion = () =>{
        this.setState({current: this.state.current + 1})
    }

    onAnswer = (answer) =>{
        const {questions, current, qty} = this.state;
        questions[current].response = parseInt(answer);
        console.log("On Answer called")
        this.setState({responseNotification : questions[current].correctAnswer === parseInt(answer) ? "Correct" : "Incorrect"})
        if(current === qty){
            this.setState({sessionCompleted : true,sessionStarted:false })
            return;
        }
        this.incrementCurrentQuestion();

    }

    render() {
        const quantityTextbox = (
            <div>
                <TextField
                    id="standard-name"
                    label="Quantity"
                    name="qty"
                    type='number'
                    value={this.state.qty}
                    onChange={this.handleQuantityChange}
                    margin="normal"
                />
                <br/>
                <Button variant="contained" color="primary" onClick={this.onSessionStart}>
                    Start
                </Button>
            </div>
        )
        const {questions, current, responseNotification, sessionCompleted} = this.state;
        return (
            <div>
                {/* {this.state.error === null ? <Error message={this.state.error}> : } */}
                {this.state.sessionStarted 
                    ? <Question question={questions[current]} onAnswer={this.onAnswer}/> 
                    : quantityTextbox }
                {responseNotification !== null ?
                 <SimpleSnackBar message={responseNotification}/> : <h1></h1>}
                {sessionCompleted ? <h2>Done</h2> : <h1></h1>}
            </div>
        );
    }
}
