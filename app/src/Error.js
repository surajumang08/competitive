import React from 'react';

export default Error = ({message}) => (
    <div>
        <h3>{message}</h3>
    </div>
)