import React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

export default class Register extends React.Component {

    state = {
        email: '',
        firstName: "",
        lastName: "",
        password: '',
        city: '',
        state: '',
        zipCode: '',
        registrationSuccess: false
    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        //make an API call and submit the data.
        fetch('/visitor/register', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then(res => res.json())
            // .then(response => console.log('Success:', JSON.stringify(response)))
            .then(response => this.handleResponse(response))
            .catch(error => console.error('Error:', error));
        console.log(this.state);
    }

    handleResponse = (response) => {
        console.log(response);
        this.setState({ registrationSuccess: response.status === 200 })
    }

    render() {
        const registrationForm = (
            <form>
                <TextField
                    id="standard-name"
                    label="Email"
                    name="email"
                    type='text'
                    value={this.state.email}
                    onChange={this.handleChange}
                    margin="normal"
                />
                <br />
                <TextField
                    id="standard-name"
                    label="FirstName"
                    name="firstName"
                    type='text'
                    value={this.state.firstName}
                    onChange={this.handleChange}
                    margin="normal"
                />
                <br />
                <TextField
                    id="standard-name"
                    label="LastName"
                    name="lastName"
                    type='text'
                    value={this.state.lastName}
                    onChange={this.handleChange}
                    margin="normal"
                />
                <br />
                <TextField
                    id="standard-name"
                    label="City"
                    name="city"
                    type='text'
                    value={this.state.city}
                    onChange={this.handleChange}
                    margin="normal"
                />
                <br />
                <TextField
                    id="standard-name"
                    label="State"
                    name="state"
                    type='text'
                    value={this.state.state}
                    onChange={this.handleChange}
                    margin="normal"
                />
                <br />
                <TextField
                    id="standard-name"
                    label="Zip Code"
                    name="zipCode"
                    type='text'
                    value={this.state.zipCode}
                    onChange={this.handleChange}
                    margin="normal"
                />
                <br />
                <TextField
                    id="standard-name"
                    label="Password"
                    name='password'
                    type='password'
                    value={this.state.password}
                    onChange={this.handleChange}
                    margin="normal"
                />
                <br /><br />
                <Button variant="contained" color="primary" onClick={this.handleSubmit}>
                    Register
                    </Button>
            </form>
        );
        return (
            <div>
                {this.state.registrationSuccess ? <h2>Registration Succesful, Try logging in</h2>
                    : registrationForm}
            </div>
        )
    }
}