import React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

export default class Login extends React.Component {

    handleChange = (e) => {
        this.setState({[e.target.name] : e.target.value})
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state);
        const data = this.state;
        fetch(`/visitor/login?email=${encodeURIComponent(data.email)}&password=${encodeURIComponent(data.password)}`, {
            method: 'GET',
        }).then(res => res.json())
        .then(this.setLoggedIn)
        .catch(error => console.error('Error:', error));
        console.log('Api returned ')
    }

    setLoggedIn = (response) =>{
        console.log(response)
        response.status === 200 ?
            this.props.setLoggedIn(true, response.message.authToken, response.message.firstName)
            : this.setState({loginFailed : true})
    }
    
    state={
        email : '',
        password : '',
        loginFailed : false
    }

    render() {
        
        return (
            <div>
                {this.state.loginFailed ? <h2>Login failed</h2> : <div> </div>}
                <form>
                    <TextField
                        id="standard-name"
                        label="Email"
                        name="email"
                        type='text'
                        value={this.state.email}
                        onChange={this.handleChange}
                        margin="normal"
                    />
                    <br/>
                    <TextField
                        id="standard-name"
                        label="Password"
                        name='password'
                        type='password'
                        value={this.state.password}
                        onChange={this.handleChange}
                        margin="normal"
                    />
                    <br/>
                    <br/>
                    <Button variant="contained" color="primary" onClick={this.handleSubmit}>
                        Login
                    </Button>
                </form>
            </div>
        )
    }
}