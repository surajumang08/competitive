import React from 'react';
import Register from './Register';
import Login from './Login';
import VisitorNav from './VisitorNav';

/*
This will show the registration page by default, but when the user clicks Login 
It should show the Login fields instead.
 */

class VisitorHome extends React.Component {
    state = {
      isLoading: true,
      questions : [{"prettyPrinted" : "Default Value"}],
      isLogin: true
    };
  
    // componentDidMount() {
    //   const response = await fetch('/questions/default');
    //   const body = await response.json();
    //   console.log(typeof body);
    //   this.setState({ questions: body, isLoading: false });
    // }

    handleClick = (page) => {
        this.setState({isLogin : page=='login' ? true : false}) 
    }
  
    render(){
      const { isLogin} = this.state
      const {setLoggedIn} = this.props;
      return (
        <div className="App">
          <VisitorNav handleClick={this.handleClick}/>
          <header className="App-header">
            {isLogin? <Login setLoggedIn={setLoggedIn}/> : <Register/>}
          </header>
        </div>
      );
    } 
  }
  
  export default VisitorHome;
  