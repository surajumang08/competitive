import React from 'react';
import logo from './logo.svg';
import './App.css';
import VisitorHome from './visitor/VisitorHome';
import MemberHome from './member/MemberHome';
import MyAppBar from './MyAppBar';

// todo : Add a nav bar which will be available across the pages.
class App extends React.Component {
  state = {
    isLoading: true,
    questions: [{ "prettyPrinted": "Default Value" }],
    isLoggedIn: false,
    authToken: '',
    firstName: ''
  };

  async componentDidMount() {
    if (localStorage.getItem('authToken') !== null) {
      this.setState({ 
        authToken: localStorage.getItem('authToken'),
        isLoggedIn: true,
        firstName: localStorage.getItem('firstName') 
      })
    }
  }

  setLoggedIn = (val, authToken, firstName) => {
    if (val) {
      localStorage.setItem('authToken', authToken);
      localStorage.setItem('firstName', firstName)
      this.setState({ isLoggedIn: val, authToken: authToken, firstName: firstName })
    } else {
        this.logout();
    }
  }

  getFirstName = () => {
    return this.state.firstName;
  }

  logout = () => {
    localStorage.removeItem('authToken');
    localStorage.removeItem('firstName')
    this.setState({ isLoggedIn: false, authToken : null, firstName:''})
  }

  render() {
    const { questions, isLoggedIn } = this.state
    return (
      <div className="App">
        <header >
          <MyAppBar />
          {isLoggedIn ? <MemberHome firstName={this.getFirstName} logout={this.logout} /> 
                      : <VisitorHome setLoggedIn={this.setLoggedIn} />}

        </header>
      </div>
    );
  }
}

export default App;
