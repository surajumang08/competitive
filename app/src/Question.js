import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

function Question({question, onAnswer}) {
  const [response, setResponse] = useState(0);
  if(!question){
    question= {
    
      prettyPrinted: "Default value"
    }

  }
  return (

    <div className="question">
        <h3>{question.prettyPrinted}</h3>
        <br/>
        <br/>
        <TextField
            id="standard-name"
            label="Response"
            name="response"
            type='number'
            value={response}
            onChange={(e) => setResponse(e.target.value)}
            margin="normal"
        />
        <br/>
        <Button variant="contained" color="primary" onClick={() => onAnswer(response)}>
            Done
        </Button>
    </div>
  );
}

export default Question;
