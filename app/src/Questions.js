import React from 'react';
import Question from './Question';

function Questions({questions}) {
  return (
    <div className="questions">
        {
            questions.map(element => 
              <Question question={element} />  
            )
        }
    </div>
  );
}

export default Questions;
