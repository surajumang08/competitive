/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.model.member;

import com.orange.competitive.model.*;
import com.orange.competitive.model.auth.Token;
import com.orange.competitive.model.report.Report;
import com.orange.competitive.model.subscription.Subscription;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Created 7/16/2019
 *
 * @author sjkumar
 */
@Getter @Setter
@Entity(name = "MEMBER")
public class Member extends BaseModel implements EmptyObjectCreatable<Member> {
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Column(name = "LAST_NAME")
    private String lastName;
    @Column(name = "EMAIL")
    private String email;
    @Embedded
    private Address address;
    @Embedded
    private Authentication authentication;

    /* ***************************************************************/
    @OneToMany(mappedBy = "member", cascade = CascadeType.ALL)
    private List<Report> reports ;
    @OneToMany(mappedBy = "member", cascade = CascadeType.ALL)
    private List<Subscription> subscriptions ;
    @OneToMany(mappedBy = "member", cascade = CascadeType.ALL)
    private List<Token> tokens ;

    public Member() {
    }

    public Member(Long id){
        setId(id);
    }

    public Member(Long id, String firstName, String lastName, String email) {
        this.setId(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    @Override
    public Member emptyObject() {
        return null;
    }
}
