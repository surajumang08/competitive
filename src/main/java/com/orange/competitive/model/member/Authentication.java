/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.model.member;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created 7/16/2019
 *
 * @author sjkumar
 */
@Getter
@Setter
@Embeddable
public class Authentication {

    @Column(name = "E_PASSWORD")
    private String password;
    @Column(name = "LOGIN_FAILURE_COUNT")
    private Integer loginFailureCount;

    public Authentication() {
    }

}
