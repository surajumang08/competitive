/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.model.report;

import com.orange.competitive.model.BaseModel;
import com.orange.competitive.model.member.Member;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created 7/16/2019
 *
 * @author sjkumar
 */
@Getter
@Setter
@Entity(name = "REPORT")
public class Report extends BaseModel {
    @ManyToOne
    @JoinColumn(name = "MEMBER_ID")
    private Member member;
    @Column(name = "TOTAL_QUESTIONS")
    private Integer totalQuestions;
    @Column(name = "CORRECT")
    private Integer correct;
    @Column(name = "ACCURACY")
    private Double accuracy;
    @Column(name = "AVERAGE_TIME")
    private Double averageTime;

    public Report() {
    }
}
