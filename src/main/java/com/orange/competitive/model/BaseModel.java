/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.model;

import javax.persistence.*;

/**
 * Created 7/17/2019
 *
 * @author sjkumar
 */
@MappedSuperclass
public abstract class BaseModel implements HibernateEntity {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) @Column(name = "ID")
    private Long id;

    protected BaseModel() {
    }

    @Override
    public final Long getId() {
        return id;
    }

    @Override
    public final void setId(final Long id) {
        this.id = id;
    }
}
