/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.model.auth;

import com.orange.competitive.model.BaseModel;
import com.orange.competitive.model.member.Member;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created 7/16/2019
 *
 * @author sjkumar
 */
@Getter @Setter
@Entity(name = "TOKEN")
public class Token extends BaseModel {

    public enum TokenType{
        EMAIL_VERIFICATION, PASSWORD_RESET, AUTH_TOKEN
    }
    @ManyToOne
    @JoinColumn(name = "MEMBER_ID")
    private Member member;
    @Column(name = "TOKEN_TYPE", columnDefinition = "ENUM('EMAIL_VERIFICATION', 'PASSWORD_RESET', 'AUTH_TOKEN')")
    @Enumerated()
    private TokenType tokenType;
    @Column(name = "EXPIRY_DATE")
    private LocalDateTime expiryDate;
    @Column(name = "SECRET")
    private String secret;

    public Token() {
    }
}
