/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.model.subscription;

import com.orange.competitive.model.BaseModel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created 7/16/2019
 *
 * @author sjkumar
 */
@Getter @Setter
@Entity(name = "PAYMENT")
public class Payment extends BaseModel {
    public enum PaymentMethod{
        DEBIT_CARD, CREDIT_CARD, UPI, NET_BANKING, PAY_PAL
    }
    @Column(name = "AMOUNT")
    private Long amount;
    @Column(name = "PAYMENT_METHOD", columnDefinition = "ENUM('DEBIT_CARD', 'CREDIT_CARD', 'UPI', 'NET_BANKING', 'PAY_PAL')")
    @Enumerated
    private PaymentMethod paymentMethod;
    @Column(name = "PAYMENT_TIME")
    private LocalDateTime paymentTime;
    @OneToOne
    @JoinColumn(name = "SUBSCRIPTION_ID")
    private Subscription subscription;
    @Transient
    private Receipt receipt;

    public Payment() {
    }

}
