/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.model.subscription;

import com.orange.competitive.model.BaseModel;
import com.orange.competitive.model.member.Member;
import com.orange.competitive.model.subscription.Payment;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created 7/16/2019
 *
 * @author sjkumar
 */

@Getter @Setter
@Entity(name = "SUBSCRIPTION")
public class Subscription extends BaseModel {

    public enum SubscriptionType{
        MONTHLY, TRI_MONTHLY
    }
    @ManyToOne
    @JoinColumn(name = "MEMBER_ID")
    private Member member;
    @Column(name = "SUBSCRIPTION_TYPE",columnDefinition = "ENUM('MONTHLY', 'TRI_MONTHLY')")
    @Enumerated
    private SubscriptionType subscriptionType;
    @Column(name = "VALID_UNTO")
    private LocalDateTime validUnto;
    @OneToOne(mappedBy = "subscription")
    private Payment payment;
    @Column(name = "START_DATE")
    private LocalDateTime startDate;

    public Subscription() {
    }

}
