/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Created 7/18/2019
 *
 * @author sjkumar
 */
public class DigestGenerator {

    private static final Logger logger = LoggerFactory.getLogger(DigestGenerator.class);
    SecureRandom secureRandom = new SecureRandom();
    MessageDigest messageDigest;

    private static final DigestGenerator digestGenerator = new DigestGenerator();
    public static DigestGenerator getInstance(){
        return digestGenerator;
    }

    private DigestGenerator() {
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            logger.info("No digest Algorithm found ", e);
        }
    }

    public String getDigest(String message){
        return getDigest(message.getBytes());
    }

    public String getDigest(byte[] bytes){
        return encodeHexString(messageDigest.digest(bytes));
    }

    //for issuing as authToken
    public String getRandomDigest(){
        byte[] random = new byte[255];
        secureRandom.nextBytes(random);
        return getDigest(random);
    }

    public String encodeHexString(byte[] byteArray) {
        StringBuilder hexStringBuffer = new StringBuilder();
        for (byte b : byteArray) {
            hexStringBuffer.append(byteToHex(b));
        }
        return hexStringBuffer.toString();
    }

    private String byteToHex(byte num) {
        char[] hexDigits = new char[2];
        hexDigits[0] = Character.forDigit((num >> 4) & 0xF, 16);
        hexDigits[1] = Character.forDigit((num & 0xF), 16);
        return new String(hexDigits);
    }
}
