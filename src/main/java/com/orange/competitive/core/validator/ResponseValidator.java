/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.core.validator;

import com.orange.competitive.core.Question;
import com.orange.competitive.core.Response;

import java.util.function.Predicate;

/**
 * Created 6/20/2019
 *
 * @author sjkumar
 */
public interface ResponseValidator extends Predicate<Response> {
    Long getCorrectResponse(Question question);
}
