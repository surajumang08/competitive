/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.core.handler;

import com.orange.competitive.core.Question;
import com.orange.competitive.core.Response;
import com.orange.competitive.core.validator.*;

import java.util.Scanner;

/**
 * Created 6/20/2019
 *
 * @author sjkumar
 */
public abstract class BasicResponseHandler implements ResponseHandler {

    protected ResponseValidator responseValidator;
    private static final Scanner scanner = new Scanner(System.in);

    public BasicResponseHandler(ResponseValidator responseValidator) {
        this.responseValidator = responseValidator;
    }

    @Override
    public String getCorrectResponseMessage(Question question) {
        return "CORRECT ANSWER : " + responseValidator.getCorrectResponse(question);
    }

    @Override
    public String getIncorrectMessage() {
        return "INCORRECT";
    }

    @Override
    public String getCorrectMessage() {
        return "CORRECT";
    }

    @Override
    public Response readUserResponse(Question question) {
        return Response.create(question, scanner.nextLong());
    }
}
