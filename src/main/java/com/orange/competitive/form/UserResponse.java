/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.form;

import com.orange.competitive.core.Question;
import com.orange.competitive.core.Response;
import com.orange.competitive.model.EmptyObjectCreatable;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import static com.orange.competitive.util.ModelUtils.EMPTY_LONG;

/**
 * Created 7/17/2019
 *
 * @author sjkumar
 */
@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class UserResponse implements EmptyObjectCreatable<UserResponse> {

    @NotNull @Pattern(regexp = "[0-9]+")
    private Long id;
    @NotNull @Pattern(regexp = "[0-9]+")
    private Long left;
    @NotNull @Pattern(regexp = "[0-9]+")
    private Long right;
    @NotNull @Pattern(regexp = "[+-/*]")
    private String operator;
    private String prettyPrinted;
    @NotNull @Pattern(regexp = "[0-9]+")
    private Long response;
    @NotNull @Pattern(regexp = "[0-9]+")
    private Long correctAnswer;

    public Response createResponse(){
        return Response.create(Question.create(left, right, Question.Operator.valueOf(operator)), response);
    }

    public static UserResponse create(Question question, long id){
        return UserResponse.builder()
                .id(id)
                .left(question.getLeft())
                .right(question.getRight())
                .operator(question.getOperator().name())
                .prettyPrinted(question.getPrettyPrinted())
                .correctAnswer(question.getCorrectAnswer())
                .build();
    }

    @Override
    public UserResponse emptyObject() {
        return UserResponse.builder().left(EMPTY_LONG)
                .right(EMPTY_LONG)
                .operator("+")
                .response(EMPTY_LONG)
                .build();
    }
}
