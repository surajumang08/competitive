/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.form;

import com.orange.competitive.core.DigestGenerator;
import com.orange.competitive.model.member.Address;
import com.orange.competitive.model.member.Authentication;
import com.orange.competitive.model.member.Member;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created 7/17/2019
 *
 * @author sjkumar
 */
@Getter @Setter
public class RegistrationForm {
    @NotNull @Pattern(regexp = "[a-zA-Z]+")
    private String firstName;

    @NotNull @Pattern(regexp = "[a-zA-Z]+")
    private String lastName;

    @NotNull
    private String email;

    @NotNull @Pattern(regexp = "[a-zA-Z]+")
    private String city;

    @NotNull @Pattern(regexp = "[a-zA-Z]+")
    private String state;

    @NotNull @Pattern(regexp = "[0-9]{6}")
    private String zipCode;

    @NotNull @Size(min = 5)
    private String password;

    public Member createMember(){
        Member member = new Member();

        member.setFirstName(firstName);
        member.setLastName(lastName);
        member.setEmail(email);
        member.setAddress(createAddress());
        member.setAuthentication(createAuthentication());
        return member;
    }

    private Address createAddress(){
        Address address = new Address();
        address.setZipCode(zipCode);
        address.setCity(city);
        address.setCountry("India");
        address.setState(state);
        return address;
    }

    private Authentication createAuthentication(){
        Authentication authentication = new Authentication();
        authentication.setPassword(DigestGenerator.getInstance().getDigest(password));
        authentication.setLoginFailureCount(0);
        return authentication;
    }
}
