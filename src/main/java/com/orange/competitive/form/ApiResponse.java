/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.form;

/**
 * Created 7/20/2019
 *
 * @author sjkumar
 */
public class ApiResponse {
    private int status;
    private Object message;

    public static final ApiResponse ok = new ApiResponse(200);

    public ApiResponse(int status) {
        this.status = status;
    }

    public ApiResponse(int status, Object message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public Object getMessage() {
        return message;
    }
}
