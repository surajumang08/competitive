/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.util;

/**
 * Created 7/17/2019
 *
 * @author sjkumar
 */
public class ModelUtils {
    private ModelUtils(){

    }

    public static final int EMPTY_INT = -128;
    public static final long EMPTY_LONG = EMPTY_INT;
    public static final String EMPTY_STRING = "";
    public static final double EMPTY_DOUBLE = -128.128;

}
