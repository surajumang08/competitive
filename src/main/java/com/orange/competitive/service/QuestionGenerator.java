/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.service;


import com.orange.competitive.core.Question.Operator;
import com.orange.competitive.core.Question;

import java.util.Random;

/**
 * Created 6/20/2019
 *
 * @author sjkumar
 */

public class QuestionGenerator {
    private Long digitsLeft;
    private Long digitsRight;
    private Operator operator;

    private static final Random random = new Random();

    public QuestionGenerator(Long digitsLeft, Long digitsRight, Operator operator) {
        this.digitsLeft = digitsLeft;
        this.digitsRight = digitsRight;
        this.operator = operator;
    }

    public static QuestionGenerator create(Long digitsLeft, Long digitsRight, Operator operator){
        return new QuestionGenerator(digitsLeft, digitsRight, operator);
    }

    public Question getNext(){
        return new Question(getRandomLong(digitsLeft), getRandomLong(digitsRight), operator);
    }

    private static Long getRandomLong(Long digits){
        long mod = (long) Math.pow(10, digits);
        return Math.abs(random.nextLong() % mod);
    }
}
