/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.controller;

import com.orange.competitive.core.DigestGenerator;
import com.orange.competitive.form.ApiResponse;
import com.orange.competitive.form.AuthToken;
import com.orange.competitive.model.auth.Token;
import com.orange.competitive.model.member.Member;
import com.orange.competitive.repository.MemberRepository;
import com.orange.competitive.repository.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

/**
 * Created 7/17/2019
 *
 * @author sjkumar
 */
@RestController
@RequestMapping("/visitor")
public class VisitorController {

    @Autowired
    MemberRepository memberRepository;
    DigestGenerator digestGenerator = DigestGenerator.getInstance();
    @Autowired
    TokenRepository tokenRepository;

    /*  Recipie
      0) Change Database column for TokenType, (Type of Sql secret column) [done]
    * 1) Registration with making a digest out of password [done]
    * 2) Login with converting the password to digest and comparing it with stored [done]
    * 3) Provide a authToken with a validity of 1 day   [done]
    * 4) Check the presence of AUTH_TOKEN in the header for every request not starting with '/visitor' todo
    * 5) Token type not being set properly [todo]
    * 6) Create a ApIContext like class which will have the currently logged in member [todo]
    * */

    @GetMapping("/login")
    public ResponseEntity<ApiResponse> login(@RequestParam String email, @RequestParam String password){
        Member member = memberRepository.findByEmail(email).orElseThrow(RuntimeException::new);
        if (member.getAuthentication().getPassword().equals(digestGenerator.getDigest(password))){
            // persist a token object as well.
            Token token = new Token();
            token.setMember(member);
            token.setExpiryDate(LocalDateTime.now().plusDays(1L));
            token.setSecret(digestGenerator.getRandomDigest());
            token.setTokenType(Token.TokenType.AUTH_TOKEN);

            tokenRepository.save(token);
            return ResponseEntity.ok(new ApiResponse(200, AuthToken.builder()
                    .id(member.getId())
                    .firstName(member.getFirstName())
                    .authToken(token.getSecret())
                    .build()));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
}
