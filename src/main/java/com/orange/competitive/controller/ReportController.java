/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.controller;

import com.orange.competitive.model.member.Member;
import com.orange.competitive.model.report.Report;
import com.orange.competitive.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

/**
 * Created 7/17/2019
 *
 * @author sjkumar
 */
@RestController
@RequestMapping("/reports")
public class ReportController {

    @Autowired
    private ReportRepository reportRepository;

    @GetMapping("/leaderboard")
    public List<Report> getLeaderBoard(){
        return Collections.emptyList();
    }

    @GetMapping("/{id}")
    public List<Report> getAllReports(@PathVariable Long id){
        return reportRepository.findAllByMember(new Member(id));
    }
}
