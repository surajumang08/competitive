/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.controller;

import com.orange.competitive.form.ApiResponse;
import com.orange.competitive.form.RegistrationForm;
import com.orange.competitive.model.member.Member;
import com.orange.competitive.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


/**
 * Created 7/17/2019
 *
 * @author sjkumar
 */
@RestController
@RequestMapping("/visitor")
public class RegistrationController {

    @Autowired
    private MemberRepository memberRepository;

    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ApiResponse> register(@Valid @RequestBody RegistrationForm registrationForm){
        Member member = registrationForm.createMember();
        memberRepository.save(member);

        return ResponseEntity.status(200).body(ApiResponse.ok);
    }
}
