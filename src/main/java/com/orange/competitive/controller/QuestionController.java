/*
 * Copyright 2006-2019 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package com.orange.competitive.controller;

import com.orange.competitive.form.ApiResponse;
import com.orange.competitive.form.UserResponse;
import com.orange.competitive.core.Question;
import com.orange.competitive.service.QuestionGenerator;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created 7/16/2019
 *
 * @author sjkumar
 */
@RestController
public class QuestionController {

    private final QuestionGenerator questionGenerator = new QuestionGenerator(2l, 2l, Question.Operator.SUM);

    @GetMapping("/questions/default")
    public ResponseEntity<ApiResponse> getQuestions(@RequestParam(value = "qty", defaultValue = "10") int quantity){
        List<UserResponse> questions = new ArrayList<>();
//        Stream.iterate()
        quantity = (quantity > 100) ? 100 : quantity;
        for (int i = 0; i < quantity; i++) {
            questions.add(UserResponse.create(questionGenerator.getNext(), i+1));
        }
        return ResponseEntity.ok(new ApiResponse(200, questions));
    }

    @PostMapping(value = "/questions/submit", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> submitResponse(@Valid @RequestBody List<UserResponse> userResponses){
        userResponses.forEach(UserResponse::createResponse);
        return ResponseEntity.ok("Received" + userResponses.size());
    }
}
